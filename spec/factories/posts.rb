# frozen_string_literal: true
require 'faker'
I18n.reload!

FactoryBot.define do
  factory :post do
    username Faker::Name.name
    title Faker::Book.title
    body Faker::Hipster.sentence(3)
  end
end
