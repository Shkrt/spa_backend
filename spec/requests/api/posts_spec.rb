# frozen_string_literal: true
describe 'api/posts' do
  it 'sends a collection of posts' do
    create_list(:post, 10)
    get '/api/posts'
    json = JSON.parse(response.body)
    expect(response).to be_success
    expect(json).to be_instance_of(Array)
    expect(json.length).to eq(10)
  end

  it 'sends a single post' do
    post = create(:post)
    get "/api/posts/#{post.id}"
    json = JSON.parse(response.body)
    expect(response).to be_success
    expect(json).to be_instance_of(Hash)
    expect(json['body']).to eq(post.body)
  end

  it 'creates post if given params are correct' do
    post '/api/posts/', params: { "post": { "title": 'Hello', "username": 'davetoxa', body: 'post_body' } }
    json = JSON.parse(response.body)
    expect(response).to be_success
    expect(response.location).to eq("#{request.protocol}#{request.host}/api/posts/#{json['id']}")
    expect(json).to be_instance_of(Hash)
    expect(json['title']).to eq('Hello')
  end

  it 'returns 422 if post is not created' do
    post '/api/posts/', params: { "post": { "title": 'Hello', "username": 'davetoxa' } }
    json = JSON.parse(response.body)
    expect(response).to have_http_status(:unprocessable_entity)
    expect(json['body'].include?("can't be blank")).to be_truthy
  end

  it 'updates post if provided params are correct' do
    post = create(:post)
    patch "/api/posts/#{post.id}", params: { "post": { "title": 'new title' } }
    json = JSON.parse(response.body)
    expect(response).to be_success
    expect(json['title']).to eq('new title')
  end

  it 'returns 422 if post is not updated' do
    post = create(:post)
    patch "/api/posts/#{post.id}", params: { "post": { "body": '' } }
    json = JSON.parse(response.body)
    expect(response).to have_http_status(:unprocessable_entity)
    expect(json['body'].include?("can't be blank")).to be_truthy
  end

  it 'destroys post' do
    post = create(:post)
    delete "/api/posts/#{post.id}"
    expect(response).to be_success
    expect(response.body).to be_blank
  end
end
